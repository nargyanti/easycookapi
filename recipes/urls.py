from django.urls import path
from .views import RecipeDetailView, SearchRecipeView, ReviewView, RecipeListView, OwnedIngredientView, NotOwnedIngredientViews, RawIngredientView, UnitView

urlpatterns = [
    path('', RecipeListView.as_view(), name='recipe_list'),
    path('raw-material/', RawIngredientView.as_view(), name='get_raw_material'),
    path('<int:recipe_id>/own/',
         OwnedIngredientView.as_view(), name='own_ingredient'),
    path('<int:recipe_id>/not-own/', NotOwnedIngredientViews.as_view(),
         name='not_own_ingredient'),
    path('<int:recipe_id>/', RecipeDetailView.as_view(), name='recipe_detail'),
    path('<int:recipe_id>/reviews/', ReviewView.as_view(), name='recipe_reviews'),
    path('<int:recipe_id>/reviews/<int:review_id>/',
         ReviewView.as_view(), name='recipe_review'),
    path('search/', SearchRecipeView.as_view(), name='recipe_search'),
    path('units/', UnitView.as_view(), name='recipe_search'),
]
