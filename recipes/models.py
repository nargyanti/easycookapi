from django.db import models
from django.contrib.auth.models import User
from datetime import datetime


class BaseIngredient(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Unit(models.Model):
    UNIT_TYPES = [
        ('measured', 'measured'),
        ('informal', 'informal'),
    ]

    UNIT_CATEGORIES = [
        ('weight', 'weight'),
        ('volume', 'volume'),
        ('length', 'length'),
        ('item', 'item'),
    ]

    code = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    hierarchy = models.IntegerField(null=True)
    category = models.CharField(
        max_length=255, choices=UNIT_CATEGORIES, null=True)
    type = models.CharField(max_length=20, choices=UNIT_TYPES, null=True)

    def __str__(self):
        return self.name


class UnitConversion(models.Model):
    higher_unit = models.ForeignKey(
        Unit, on_delete=models.CASCADE, related_name='higher_unit')
    lower_unit = models.ForeignKey(
        Unit, on_delete=models.CASCADE, related_name='lower_unit')
    value = models.IntegerField(null=True)


class Information(models.Model):
    title = models.CharField(max_length=255)
    image_url = models.CharField(max_length=255)
    time = models.IntegerField(null=True)
    servings = models.IntegerField(null=True)
    calories = models.IntegerField(null=True)
    difficulty = models.CharField(max_length=255, null=True)
    instructions = models.TextField()
    source_url = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class RawIngredient(models.Model):
    name = models.CharField(max_length=255)
    base = models.ForeignKey(BaseIngredient, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    recipe = models.ForeignKey(
        Information, on_delete=models.CASCADE, related_name='ingredients')
    name = models.CharField(max_length=255)
    amount = models.FloatField(null=True)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE, null=True)
    state = models.CharField(max_length=255, null=True)
    raw_ingredient = models.ForeignKey(RawIngredient, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Tag(models.Model):
    recipe = models.ForeignKey(Information, on_delete=models.CASCADE)
    tag = models.CharField(max_length=255)

    def __str__(self):
        return self.tag


class Review(models.Model):
    recipe = models.ForeignKey(
        Information, on_delete=models.CASCADE, related_name='reviews')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.TextField(null=True, blank=True, default='')
    rating = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user.username} - {self.recipe.title}'


class ReviewImage(models.Model):
    review = models.ForeignKey(Review, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='reviews')

    def __str__(self):
        return f'Image for {self.review}'


def user_directory_path(instance, filename):
    filename = str(datetime.now()) + ".png"
    return 'profile_picts/{1}'.format(instance.user.id, filename)
