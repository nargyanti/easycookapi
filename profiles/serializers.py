from django.contrib.auth.models import User
from rest_framework import serializers
from profiles.models import Account, Favorite, History, Pantry


class FavoriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Favorite
        fields = '__all__'


class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username', 'email']


class AccountSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=False)

    class Meta:
        model = Account
        fields = ['user', 'profile_picture', 'phone']


class ProfilePictureSerializer(AccountSerializer):
    class Meta(AccountSerializer.Meta):
        fields = ['profile_picture']


class PantrySerializer(serializers.ModelSerializer):
    unit = serializers.CharField(source='unit.code', allow_null=True)
    raw_ingredient = serializers.CharField(source='raw_ingredient.name')

    class Meta:
        model = Pantry
        fields = '__all__'
