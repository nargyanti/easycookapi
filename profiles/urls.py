from django.urls import path
from profiles.views import (AccountView, HistoryView, FavoriteView, PantryView,
                            UpdatePasswordView, PantryRecommendationView, CFRecommendationView, HybridRecommendationView, MostViewedRecommendationView, FavoriteRecommendationView)

urlpatterns = [
    # URL's for Favorite
    path('favorites/', FavoriteView.as_view(), name="favorites"),
    path('favorites/<int:recipe_id>/', FavoriteView.as_view(), name="favorite"),

    # URL's for History
    path('history/', HistoryView.as_view(), name="histories"),
    path('history/<int:recipe_id>/', HistoryView.as_view(), name="history"),


    # URL's for the Account
    path('account/', AccountView.as_view(), name='account'),
    path('account/update/', AccountView.as_view(), name='account-update'),
    path('account/password/update',
         UpdatePasswordView.as_view(), name='password-update'),

    # URL's for the Pantry
    path('pantry/', PantryView.as_view(), name='pantry-list'),
    path('pantry/<int:pantry_id>',
         PantryView.as_view(), name='pantry-list'),
    path('pantry/recommendations/', PantryRecommendationView.as_view(),
         name='pantry-recommendation'),
    path('cf_recommendations/',
         CFRecommendationView.as_view(), name='recommendations'),
    path('hybrid_recommendations/',
         HybridRecommendationView.as_view(), name='recommendations'),
    path('most_viewed_recommendations/',
         MostViewedRecommendationView.as_view(), name='recommendations'),
    path('favorited_recommendations/',
         FavoriteRecommendationView.as_view(), name='recommendations'),

]
