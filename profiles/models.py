from django.db import connections, models
from django.contrib.auth.models import User
from recipes.models import Information, RawIngredient, BaseIngredient, Unit
from datetime import datetime


def user_directory_path(instance, filename):
    return 'profile_picts/{user_id}_{filename}'.format(user_id=instance.user.id, filename=str(datetime.now()) + ".png")


class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_picture = models.ImageField(
        default='default_profpics.png', upload_to=user_directory_path)
    phone = models.CharField(max_length=20, unique=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user.username} Profile'


class Favorite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    recipe = models.ForeignKey(Information, on_delete=models.CASCADE)
    source = models.CharField(max_length=30, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class History(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    recipe = models.ForeignKey(Information, on_delete=models.CASCADE)
    source = models.CharField(max_length=20, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class Pantry(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    raw_ingredient = models.ForeignKey(RawIngredient, on_delete=models.CASCADE)
    amount = models.IntegerField(null=True, default=None)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class Restriction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    base_ingredient = models.ForeignKey(
        BaseIngredient, on_delete=models.CASCADE)
