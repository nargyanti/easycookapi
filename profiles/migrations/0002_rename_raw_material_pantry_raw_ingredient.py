# Generated by Django 4.1 on 2023-06-28 11:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pantry',
            old_name='raw_material',
            new_name='raw_ingredient',
        ),
    ]
