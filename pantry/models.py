from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Product(models.Model):
    id_product = models.IntegerField(null=True)
    nama_bahan = models.CharField(max_length=255)
    harga = models.FloatField(null=True)
    berat = models.IntegerField(null=True)
    satuan = models.CharField(max_length=50)
    nama_supplier = models.CharField(max_length=155)
    alamat = models.TextField(null=True)


class Rekomendasi(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    supplier = models.CharField(max_length=255, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
