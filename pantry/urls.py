from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .views import RekomendasiView, RecipeRecomendationView, ProductView

urlpatterns = [
    path('product/', ProductView.as_view(), name="add-product"),

    path("<int:id>/rekomendasi/", RekomendasiView.as_view(),
         name="rekomendasi-supplier"),

    path("rekomendasi-resep/", RecipeRecomendationView.as_view(),
         name="rekomendasi-resep"),
]
