# Generated by Django 4.1 on 2023-06-28 09:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_product', models.IntegerField(null=True)),
                ('nama_bahan', models.CharField(max_length=255)),
                ('harga', models.FloatField(null=True)),
                ('berat', models.IntegerField(null=True)),
                ('satuan', models.CharField(max_length=50)),
                ('nama_supplier', models.CharField(max_length=155)),
                ('alamat', models.TextField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Rekomendasi',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pantry.product')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
